# reactapp

An Adapt starter that contains a React front end (created with create-react-app) and a back end with NGINX serving static files, a Node.js + Express API server, and a URL router that routes to the API server or static server.